var Config = require('./config/config.json');
var Server = require('./server/app');
var IP = require('ip');

//Standard PORT from config file or use the PORT of system environment
Config.PORT = process.env.OPENSHIFT_NODEJS_PORT || Config.PORT;
Config.IP = process.env.OPENSHIFT_NODEJS_IP || IP.address();
//Start the server
Server.start(Config);