var DBcon = require("./global/dbcon");
var CheckAuth = require("./checkAuth");
var LogConf = require('../../config/log4js-conf.json');
var log4js = require('log4js');

log4js.configure(LogConf, {});

var chatLogger = log4js.getLogger('chat');

module.exports = function (app) {

    /**
     * @api {post} /api/messages GetMessages
     * @apiName getMessages
     * @apiGroup Chat
     * @apiVersion 0.1.0
     *
     * @apiParam {String} room_nr  Meeting Room Number
     *
     * @apiSuccess {Int} id User ID
     * @apiSuccess {String} email Email address of the user
     * @apiSuccess {String} message The chat message
     * @apiSuccess {String} room_nr Meeting Room Number
     * @apiSuccess {DateTime} createdAt DB insert date
     * @apiSuccess {DateTime} updatedAt DB update date
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     * "messages": [
     * {
     * "id": 1,
     * "email": "m@m.com",
     * "message": "Hi",
     * "room_nr": "ac6ca38b-f37f-4e7f-9da9-08920811eac7",
     * "createdAt": "2016-01-25T18:28:32.000Z",
     * "updatedAt": "2016-01-25T18:28:32.000Z"
     * }
     * ]
     * }
     *
     * @apiError NoPreviousMessages The <code>room_nr</code> does not have any chat messages.
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 400 Bad Request
     * {
     * error: "No data found"
     * }
     */
    app.post("/api/messages", CheckAuth, function (req, res) {
        var room_nr = req.body.room_nr;

        DBcon.models.Chat.findAll({where: {room_nr: room_nr}})
            .then(function (messages) {
                if(messages != 0){
                    chatLogger.info('Previous messages provided for the user: '+req.session.user.id+' for the room: '+room_nr);
                    res.status(200).send({
                        messages: messages
                    });
                } else {
                    chatLogger.warn('No data found in the previous messages. Asked by: '+req.session.user.id+' for the room: '+room_nr);
                    res.status(400).send({
                        error: "No data found"
                    });
                }
            })
            .catch (function() {
            res.status(400).send({
                error: "No data found"
            })
        });
    });

    /**
     * @api {post} /api/messageInsert Insert Message
     * @apiName insertMessage
     * @apiGroup Chat
     * @apiVersion 0.1.0
     *
     * @apiParam {String} email  Email address of the user
     * @apiParam {String} message  Chat message
     * @apiParam {String} room_nr  Meeting Room Number
     *
     * @apiSuccess {Int} id User ID
     * @apiSuccess {String} email Email address of the user
     * @apiSuccess {String} message The chat message
     * @apiSuccess {String} room_nr Meeting Room Number
     * @apiSuccess {DateTime} createdAt DB insert date
     * @apiSuccess {DateTime} updatedAt DB update date
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     * "messages": [
     * {
     * "id": 1,
     * "email": "m@m.com",
     * "message": "Hi",
     * "room_nr": "ac6ca38b-f37f-4e7f-9da9-08920811eac7",
     * "createdAt": "2016-01-25T18:28:32.000Z",
     * "updatedAt": "2016-01-25T18:28:32.000Z"
     * }
     * ]
     * }
     *
     * @apiError DoubleMessage The requested data is not unique.
     */
    app.post("/api/messageInsert", CheckAuth, function (req, res) {
        var message = {
            email: req.body.email,
            message: req.body.message,
            room_nr: req.body.room_nr
        }
        DBcon.models.Chat.create(message)
            .then(function (createdMessage) {
                res.send(createdMessage);
            })
            .catch (function(err) {
            res.status(400).send({
                error: err
            });
        })
    });
};