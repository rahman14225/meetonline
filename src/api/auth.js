var DBcon = require("./global/dbcon");
var CheckAuth = require("./checkAuth");
var EmailValidator = require("email-validator");
var Validations = require("./global/validation.json");
var LogConf = require('../../config/log4js-conf.json');
var log4js = require('log4js');

log4js.configure(LogConf, {});

var loginLogger = log4js.getLogger('login');
var registrationLogger = log4js.getLogger('registration');
var changePasswordLogger = log4js.getLogger('myAccount');

module.exports = function (app) {

    var PasswordValidator = new RegExp(/^[0-9a-zA-Z]{6,128}$/);
    /**
     * @api {post} /api/auth/registration Registration
     * @apiName registration
     * @apiGroup Authentication
     * @apiVersion 0.1.0
     *
     * @apiParam {String} email  Email address of the new user
     * @apiParam {String} password SHA512 hashed password
     *
     * @apiSuccess {Int} id The created user ID
     * @apiSuccess {String} email The email address of the new user
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
	 * id: 14,
	 * email: "mrahman3@uclan.ac.uk"
	 * }
     *
     * @apiError UserAlreadyExists The <code>email</code> of the new user was found.
     * @apiErrorExample {json} Error-Response (UserAlreadyExists):
     * HTTP/1.1 400 Bad Request
     * {
     * id: 14,
	 * email: "mrahman3@uclan.ac.uk"
     * }
     *
     * * @apiError InvalidData The <code>email</code> and/or <code>password</code> of the new user was invalid.
     * @apiErrorExample {json} Error-Response (InvalidData):
     * HTTP/1.1 400 Bad Request
     * {
     * error: "Invalid email address or password"
     * }
     */
    app.post("/api/auth/registration", function (req, res) {
        var reqEmail = req.body.email;
        var reqPassword = req.body.password;

        if (EmailValidator.validate(reqEmail) && PasswordValidator.test(reqPassword)) {
            DBcon.models.User.findOne({where: {email: reqEmail}})
                .then(function (user) {
                    registrationLogger.warn('An existing user tried to register again. Email: ' + user.id);
                    res.status(400).send({
                        id: user.id,
                        email: user.email
                    });
                })
                .catch(function () {
                    return DBcon.models.User.create({
                        email: reqEmail,
                        password: reqPassword
                    }).then(function (createdUser) {
                        registrationLogger.info('Registration successful. New user with email: ' + createdUser.id);
                        res.status(200).send({
                            id: createdUser.id,
                            email: createdUser.email
                        });
                    })
                })
        } else {
            registrationLogger.error('Someone tried to register with invalid data. Email: ' + reqEmail + ' Password: ' + reqPassword);
            res.status(400).send({
                error: "Invalid email address or password"
            });
        }
    });

    /**
     * @api {post} /api/auth/login Login
     * @apiName login
     * @apiGroup Authentication
     * @apiVersion 0.1.0
     *
     * @apiParam {String} email  Email address of the user
     * @apiParam {String} password SHA512 hashed password
     *
     * @apiSuccess {Int} id The user ID
     * @apiSuccess {String} email The email address of the user
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
	 * id: 14,
	 * email: "mrahman3@uclan.ac.uk"
	 * }
     *
     * @apiError WrongData The <code>email</code> and/or <code>password</code> of the user was wrong.
     * @apiErrorExample {json} Error-Response (WrongData):
     * HTTP/1.1 400 Bad Request
     * {
     * error: "Wrong email/password"
     * }
     *
     * @apiError InvalidData The <code>email</code> and/or <code>password</code> of the new user was invalid.
     * @apiErrorExample {json} Error-Response (InvalidData):
     * HTTP/1.1 400 Bad Request
     * {
     * error: "Invalid email address or password"
     * }
     */
    app.post("/api/auth/login", function (req, res) {
        var reqEmail = req.body.email;
        var reqPassword = req.body.password;

        if (EmailValidator.validate(reqEmail) && PasswordValidator.test(reqPassword)) {
            DBcon.models.User.findOne({where: {email: reqEmail, password: reqPassword}})
                .then(function (user) {
                    loginLogger.info('Login successful for the user: ' + user.email);
                    req.session.user = user;
                    res.send({
                        id: user.id,
                        email: user.email
                    });
                }).catch(function (err) {
                loginLogger.warn('An user sent a request with wrong email/password. Email: ' + reqEmail);
                res.status(400).send({
                    error: "Wrong email/password"
                });
            });
        } else {
            loginLogger.error('Someone tried to login with invalid email or password. Email: ' + reqEmail + ' Password: ' + reqPassword);
            res.status(400).send({
                error: "Invalid email address or password"
            });
        }
    });

    /**
     * @api {post} /api/changePassword Change Password
     * @apiName changePassword
     * @apiGroup Authentication
     * @apiVersion 0.1.0
     *
     * @apiParam {String} email  Email address of the new user
     * @apiParam {String} currentPassword SHA512 hashed current password
     * @apiParam {String} newPassword SHA512 hashed new password
     *
     * @apiSuccess {Int} id The user ID
     * @apiSuccess {String} email The email address of the user
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
	 * success: 'Password updated'
	 * }
     *
     * @apiError UserNotFound The <code>email</code> and/or <code>password</code> of the user was wrong.
     * @apiErrorExample {json} Error-Response (UserNotFound):
     * HTTP/1.1 400 Bad Request
     * {
     * error: 'No user found'
     * }
     *
     * @apiError InvalidData The <code>email</code> and/or <code>password</code> of the new user was invalid.
     * @apiErrorExample {json} Error-Response (InvalidData):
     * HTTP/1.1 400 Bad Request
     * {
     * error: "Invalid email address or password"
     * }
     */
    app.post("/api/changePassword", CheckAuth, function (req, res) {
        var userEmail = req.body.userEmail;
        var currentPassword = req.body.currentPassword;
        var newPassword = req.body.newPassword;

        if (EmailValidator.validate(userEmail) && PasswordValidator.test(currentPassword) && PasswordValidator.test(newPassword)) {
            DBcon.models.User.update({password: newPassword}, {where: {email: userEmail, password: currentPassword}})
                .then(function (user) {
                    if (user != 0) {
                        changePasswordLogger.info('Password updated for: ' + userEmail);
                        res.status(200).send({
                            success: 'Password updated'
                        });
                    } else {
                        changePasswordLogger.warn('No user with the email: ' + userEmail + ' found');
                        res.status(400).send({
                            error: 'No user found'
                        });
                    }
                })
                .catch(function (err) {
                    res.status(400).send({
                        error: err
                    });
                })
        } else {
            changePasswordLogger.error('Someone tried to change the password with invalid data. Email: ' + userEmail + ' CurrentPw: ' + currentPassword + ' NewPw: ' + newPassword);
            res.status(400).send({
                error: "Invalid email address or password"
            });
        }
    });

    /**
     * @api {get} /api/auth/logout Logout
     * @apiName logout
     * @apiGroup Authentication
     * @apiVersion 0.1.0
     *
     * @apiParam {Session} user User session on the server-side
     *
     * @apiSuccess {Session} user The user session was deleted
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
	 * 'logged out'
	 * }
     */
    app.get("/api/auth/logout", function (req, res) {
        loginLogger.info('User logged out. User ID: ' + req.session.user.id);
        delete req.session.user;
        res.send('logged out');
    });

    /**
     * @api {get} /api/auth/loggedIn LoggedIn
     * @apiName loggedIn
     * @apiGroup Authentication
     * @apiVersion 0.1.0
     *
     * @apiParam {Session} user User session on the server-side
     *
     * @apiSuccess {Session} user The user session is active
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
	 * 'Already logged in'
	 * }
     */
    app.get('/api/auth/loggedIn', CheckAuth, function (req, res) {
        res.send('Already logged in');
    });
};