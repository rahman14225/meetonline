var DBcon = require("./global/dbcon");
var CheckAuth = require("./checkAuth");
var LogConf = require('../../config/log4js-conf.json');
var log4js = require('log4js');

log4js.configure(LogConf, {});

var meetingLogger = log4js.getLogger('meeting');

module.exports = function (app) {

    /**
     * @api {get} /api/meetings Get Meetings
     * @apiName getMeetings
     * @apiGroup Meeting
     * @apiVersion 0.1.0
     *
     * @apiParam {Session} userID Server-side session (user ID)
     *
     * @apiSuccess {String} name Meeting Room Name
     * @apiSuccess {String} room_nr Meeting Room Number
     * @apiSuccess {String} desc Meeting Room Description
     * @apiSuccess {Int} user Bookmarked by user
     * @apiSuccess {Boolean} active Is the meeting room active?
     * @apiSuccess {DateTime} createdAt DB insert date
     * @apiSuccess {DateTime} updatedAt DB update date
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
	 * "meetings": [
     * {
     * "name": "Test Meeting",
     * "room_nr": "ac6ca38b-f37f-4e7f-9da9-08920811eac7",
     * "desc": "Test Test Test Test ",
     * "user": 1,
     * "active": 1,
     * "createdAt": "2016-01-25T18:28:32.000Z",
     * "updatedAt": "2016-01-25T18:28:32.000Z"
     * }
     * ]
	 * }
     *
     * @apiError DataNotFound There are not any bookmarked meetings for the user
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 400 Bad Request
     * {
     * error: "No data found"
     * }
     */
    app.get("/api/meetings", CheckAuth, function (req, res) {
        var userId = req.session.user.id;

        DBcon.models.Meeting.findAll({where: {user: userId}})
            .then(function (meetings) {
                meetingLogger.info('Previous meetings of user: '+userId+' has been sent');
                res.status(200).send({
                    meetings: meetings
                });
            })
            .catch (function(){
                meetingLogger.warn('There are no previous meetings of user: '+userId+' to send');
                res.status(400).send({
                    error: "No data found"
                })
            })
    });

    /**
     * @api {post} /api/meetingsActive Active Meetings
     * @apiName activeMeetings
     * @apiGroup Meeting
     * @apiVersion 0.1.0
     *
     * @apiParam {String} room_nr Meeting Room Number
     *
     * @apiSuccess {String} name Meeting Room Name
     * @apiSuccess {String} room_nr Meeting Room Number
     * @apiSuccess {String} desc Meeting Room Description
     * @apiSuccess {Int} user Bookmarked by user
     * @apiSuccess {Boolean} active Is the meeting room active?
     * @apiSuccess {DateTime} createdAt DB insert date
     * @apiSuccess {DateTime} updatedAt DB update date
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     * "name": "Test Meeting",
     * "room_nr": "ac6ca38b-f37f-4e7f-9da9-08920811eac7",
     * "desc": "Test Test Test Test ",
     * "user": 1,
     * "active": 1,
     * "createdAt": "2016-01-25T18:28:32.000Z",
     * "updatedAt": "2016-01-25T18:28:32.000Z"
	 * }
     *
     * @apiError DataNotFound The given <code>room_nr</code> not found in the DB
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 400 Bad Request
     * {
     * error: "No data found"
     * }
     */
    app.post("/api/meetingsActive", CheckAuth, function (req, res) {

        var reqNumber = req.body.room_nr;

        DBcon.models.Meeting.findOne({where: {room_nr: reqNumber, active: 1}})
            .then(function (meeting) {
                if(meeting != null){
                    res.status(200).send({
                        meeting: meeting
                    });
                } else
                {
                    res.status(400).send({
                        error: "No data found"
                    });
                }
            })
            .catch (function(){
            res.status(400).send({
                error: "No data found"
            })
        })
    });

    /**
     * @api {post} /api/meetings Bookmark meeting
     * @apiName bookmarkMeeting
     * @apiGroup Meeting
     * @apiVersion 0.1.0
     *
     * @apiParam {String} name Meeting Room Name
     * @apiParam {String} room_nr Meeting Room Number
     * @apiParam {String} desc Meeting Room Description
     * @apiParam {Session} user Server-side session user id
     *
     * @apiSuccess {String} name Meeting Room Name
     * @apiSuccess {String} room_nr Meeting Room Number
     * @apiSuccess {String} desc Meeting Room Description
     * @apiSuccess {Int} user Bookmarked by user
     * @apiSuccess {Boolean} active Is the meeting room active?
     * @apiSuccess {DateTime} createdAt DB insert date
     * @apiSuccess {DateTime} updatedAt DB update date
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     * "name": "Test Meeting",
     * "room_nr": "ac6ca38b-f37f-4e7f-9da9-08920811eac7",
     * "desc": "Test Test Test Test ",
     * "user": 1,
     * "active": 1,
     * "createdAt": "2016-01-25T18:28:32.000Z",
     * "updatedAt": "2016-01-25T18:28:32.000Z"
	 * }
     *
     * @apiError Uniqueness The requested data is not unique
     */
    app.post("/api/meetings", CheckAuth, function (req, res) {
        var meeting = {
            name: req.body.name,
            room_nr: req.body.room_nr,
            desc: req.body.desc,
            user: req.session.user.id,
            active: true
        };

        DBcon.models.Meeting.create(meeting)
            .then(function (createdMeeting) {
                meetingLogger.info('Meeting: '+createdMeeting.room_nr+' created by: '+createdMeeting.user+' (User ID) has been successfully bookmarked');
                res.send(createdMeeting);
            })
            .catch (function(err) {
                //meetingLogger.warn('Meeting: '+req.body.room_nr+' created by: '+createdMeeting.user+' (User ID) is already bookmarked');
            res.status(400).send({
                error: err
            });
        })
    });

    //Room activation control
    app.post("/api/meetingSet", CheckAuth, function (req, res) {
            var setActive = req.body.active;
            var r_nr = req.body.room_nr;


        DBcon.models.Meeting.update({active: setActive},{where: {room_nr: r_nr}})
            .then(function (data) {
                res.status(200).send(data)
            })
            .catch (function(err) {
            res.status(400).send({
                error: err
            });
        })
    });
};