var Sequelize = require('sequelize');
//DB connection
var sequelize = new Sequelize('meetonline', 'meetonline', 'test1234', {
    host: "127.0.0.1",
    dialect: "mysql"
});

/*var sequelize = new Sequelize('meetonline', 'mrahman14', 'gangster007', {
    host: "db4free.net",
    dialect: "mysql"
});*/

/*var sequelize = new Sequelize('mo', 'adminJF9nAee', '1ivwEXy_rv7d', {
 host: "mo-meetonline.rhcloud.com",
 dialect: "mysql"
 });*/

//User table
var User = sequelize.define('User', {
    email: {type: Sequelize.STRING, allowNull: false, isEmail: true, unique: true},
    password: {type: Sequelize.STRING, max: 128, allowNull: false}
});

//Meeting table
var Meeting = sequelize.define('Meeting', {
    name: {type: Sequelize.STRING, allowNull: false},
    room_nr: {type: Sequelize.STRING, allowNull: false, primaryKey: true},
    desc: Sequelize.TEXT,
    user: {type: Sequelize.INTEGER, allowNull: false, primaryKey: true},
    active: {type: Sequelize.BOOLEAN, allowNull: false}
});

//Chat table
var Chat = sequelize.define('Chat', {
    email: {type: Sequelize.STRING, allowNull: false, isEmail: true},
    message: Sequelize.TEXT,
    room_nr: {type: Sequelize.STRING, allowNull: false}
});


User.sync({force: false});

Meeting.sync({force: false});

Chat.sync({force: false});


module.exports.models = {};
module.exports.models.User = User;
module.exports.models.Meeting = Meeting;
module.exports.models.Chat = Chat;