var Nodemailer = require("nodemailer");
var MailerConfig = require("./global/mailer.json");
var SmtpTransport = require('nodemailer-smtp-transport');
var IP = require('ip');
var serverPort = require('./../../config/config.json');
var LogConf = require('../../config/log4js-conf.json');
var log4js = require('log4js');

log4js.configure(LogConf, {});

var mailerLogger = log4js.getLogger('mailer');

var smtpOptions = {
    service: MailerConfig.Service,  // sets automatically host, port and connection security settings
    auth: {
        user: MailerConfig.User,
        pass: MailerConfig.Password
    }
};

var transporter = Nodemailer.createTransport(SmtpTransport(smtpOptions));

module.exports = function (app) {

    /**
     * @api {post} /api/sendEmail Invitation Email
     * @apiName invitation
     * @apiGroup Mailer
     * @apiVersion 0.1.0
     *
     * @apiParam {String} emailTo  Recipient email address
     * @apiParam {String} meetingRoomNumber Meeting Room Number for the invitation
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
	 * 'Email sent'
	 * }
     *
     * @apiError ServerError Internal server error
     */
    app.post("/api/sendEmail", function (req, res) {

        var registrationLink = "https://"+IP.address()+":"+serverPort.PORT+"/#/registration";
        var emailData = {
            envelope: {
                from: MailerConfig.User,
                to: req.body.emailTo
            },
            subject: "Meeting Invitation", // subject
            html: "<h1>MeetOnline - Invitation</h1> " +
            "<p>This is the MeetOnline Room Number: <strong>"+ req.body.meetingRoomNumber +"</strong>. Please join over MeetOnline WebApp!</p> " +
            "<p>If you don&#39;t have a MeetOnline Account please follow the steps below:</p> " +
            "<ul> " +
            "<li>Go to the WebApp: <a href='"+registrationLink+"'>MeetOnline - Registration</a></li>" +
            "<li>Register with your email address and password</li> " +
            "<li>Login with your new account</li> " +
            "<li>Join the meeting with MeetOnline Room Number</li> " +
            "</ul> " +
            "<p>Have fun :)</p> " +
            "<blockquote> " +
            "<p>MeetOnline is a web based conference system to provide companies as well as private persons to organise their meetings online. It&#39;s completely FREE!</p> " +
            "</blockquote>"
        };
        transporter.sendMail(emailData, function (error, response) {
            if (error) {
                mailerLogger.warn('Email unable to send. Error message: '+error);
                res.status(400).send({
                    error: error
                });
            } else {
                mailerLogger.info('Invitation email sent. From: MeetOnline, To: '+req.body.emailTo);
                res.status(200).send('Email sent');
            }

            transporter.close();
        });
    });

    /**
     * @api {post} /api/sendEmailReg Registration Email
     * @apiName registrationEmail
     * @apiGroup Mailer
     * @apiVersion 0.1.0
     *
     * @apiParam {String} emailTo  Recipient email address
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
	 * 'Email sent'
	 * }
     *
     * @apiError ServerError Internal server error
     */
    app.post("/api/sendEmailReg", function (req, res) {

        var emailData = {
            envelope: {
                from: MailerConfig.User,
                to: req.body.emailTo
            },
            subject: "Welcome to MeetOnline", // subject
            html: "<h1>Welcome to MeetOnline!</h1>" +
            "<p>Thank you for joining MeetOnline. Our application is a web based conference system to provide companies as well as private persons to organise their meetings online. It&#39;s completely FREE!</p>" +
            "<p>Login and have fun!</p>" +
            "<p>Login URL: <a href='http://mo-meetonline.rhcloud.com/#/login'>MeetOnline - Login</a><br />" +
            "Email: <strong>"+req.body.emailTo+"</strong><br />" +
            "Password: *************** (your given password for the system)</p>" +
            "<p>We are here to help you</p>"+
            "<ul>"+
            "<li>Email us: <a href='mailto:rah14225@gmail.com?subject=MeetOnline%20-%20Support&amp;body=Enter%20your%20question!'>MeetOnline - Support</a> or</li>"+
            "<li>Visit our FAQs: <a href='http://mo-meetonline.rhcloud.com/#/faq'>MeetOnline - FAQs</a></li>"+
            "</ul>"+
            "<p>Cheers</p>"+
            "<p>MeetOnline Team<br />"+
            "&nbsp;</p>"
        };
        transporter.sendMail(emailData, function (error, response) {
            if (error) {
                mailerLogger.warn('Email unable to send. Error message: '+error);
                res.status(400).send({
                    error: error
                });
            } else {
                mailerLogger.info('Registration email sent. From: MeetOnline, To: '+req.body.emailTo);
                res.status(200).send('Email sent');
            }

            transporter.close();
        });
    });
};