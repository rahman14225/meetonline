//Check if the user is authorized to view the page
module.exports = function (req, res, next) {
    if (!req.session.user) {
        return res.status(401).send('Not authorized to this page');
    }
    return next();
};