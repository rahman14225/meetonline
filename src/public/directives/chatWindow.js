(function () {
    'use strict';

    angular.module('meetonline').directive('chatBox', function () {
        return {
            restrict: 'E',
            template: '<textarea class="chat-box" ng-disable="true" ng-model="messageLog" readonly disabled></textarea>',
            controller: function ($scope, $element) {
                $scope.$watch('messageLog', function () {
                    var textArea = $element[0].children[0];
                    textArea.scrollTop = textArea.scrollHeight;
                });
            }
        };
    });

}());
