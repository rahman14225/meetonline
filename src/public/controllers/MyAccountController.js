angular.module("meetonline").controller("MyAccountController", function ($scope, $http, $location, localStorageService) {
    if (!$scope.isLoggedIn()) {
        $location.path("/");
    }
    var userEmail = $scope.userEmail = localStorageService.get("user").email;
    $scope.master = {};
    $scope.isFeedback = "none";

    $scope.changePassword = function () {
        var currentPassword = $scope.myAcc.password;
        var newPassword = $scope.myAcc.newPassword;

        var currentPasswordHashValue = CryptoJS.SHA512(currentPassword);
        var newPasswordHashValue = CryptoJS.SHA512(newPassword);

        var currentPasswordHashed = currentPasswordHashValue.toString(CryptoJS.enc.Hex);
        var newPasswordHashed = newPasswordHashValue.toString(CryptoJS.enc.Hex);

        var data = {userEmail: userEmail, currentPassword: currentPasswordHashed, newPassword: newPasswordHashed};
        $http.post("/api/changePassword", data)
            .success(function (data) {
                console.log(data);
                $scope.feedbackState = "success";
                $scope.isFeedback = "block";
                $scope.feedbackMsg = "Password changed";
                $scope.myAcc = angular.copy($scope.master);
                $scope.myAccForm.$setPristine();
            })
            .error(function (data) {
                console.log(data);
                $scope.feedbackState = "danger";
                $scope.isFeedback = "block";
                $scope.feedbackMsg = "Current password is incorrect";
                $scope.myAcc = angular.copy($scope.master);
                $scope.myAccForm.$setPristine();
            });
    };
});