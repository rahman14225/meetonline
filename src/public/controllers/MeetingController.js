'use strict';

angular.module('meetonline').controller('RoomCtrl', function ($sce, VideoStream, $location, $routeParams, $scope, Room, $http, $log, chatSocket, messageFormatter, nickName, localStorageService, $uibModal) {

    if (!$scope.isLoggedIn()) {
        $location.path("/");
    }
    else {
        //Video stream
        if (!window.RTCPeerConnection || !navigator.getUserMedia) {
            $scope.error = 'WebRTC is not supported by your browser.';
            return;
        } else {

            var stream;

            VideoStream.get()
                .then(function (s) {
                    stream = s;
                    Room.init(stream);
                    stream = URL.createObjectURL(stream);

                    //Meeting Nr. for the tmpl
                    $scope.rnb = $routeParams.roomId;
                    //Button activation for invitation
                    $scope.takeAtourBtn = true;
                    $scope.invitationBtn = true;
                    $scope.saveMeetingBtn = true;

                    //store room details in the db


                    if (!$routeParams.roomId) {
                        Room.createRoom()
                            .then(function (roomId) {
                                $location.path('/room/' + roomId);
                            });
                    } else {
                        Room.joinRoom($routeParams.roomId);
                    }
                }, function () {
                    $scope.error = 'No audio/video permissions. Please refresh your browser and allow the audio/video capturing.';
                });
            $scope.peers = [];
            Room.on('peer.stream', function (peer) {
                console.log('Client connected, adding new stream ' + peer.id);
                //$scope.test = peer.id;
                $scope.peers.push({
                    id: peer.id,
                    stream: URL.createObjectURL(peer.stream)
                });
            });
            Room.on('peer.disconnected', function (peer) {
                console.log('Client disconnected, removing stream');
                $scope.peers = $scope.peers.filter(function (p) {
                    return p.id !== peer.id;
                });
            });

            $scope.getLocalVideo = function () {
                return $sce.trustAsResourceUrl(stream);
            };
        }

        // Set meeting in the DB


        // Chats
        $scope.loadBtn = false;
        $scope.submit = function(){
            var data = {room_nr: $routeParams.roomId};
            $http.post("/api/messages", data)
                .success(function (messages) {
                    $scope.messages = messages;
                    $scope.loadBtn = true;
                })
                .error(function () {
                    $scope.errorLoad = "There are no previous messages";
                    $scope.loadBtn = true;
                });
        };

        //Chat
        $scope.nickName = localStorageService.get("user").email;
        $scope.sendMessage = function() {
            if ($scope.chatMsg.$valid) {
                $log.debug('sending message', $scope.message);
                chatSocket.emit('message', localStorageService.get("user").email, $scope.message);
                var data = {email: localStorageService.get("user").email, message: $scope.message, room_nr: $routeParams.roomId};
                $http.post("/api/messageInsert", data)
                    .success(function () {
                        console.log('Message stored');
                    })
                    .error(function(){
                        console.log('Message storing failed');
                    });
                $scope.message = '';
            }
        };

        $scope.$on('socket:broadcast', function(event, data) {
            $log.debug('got a message', event.name);
            if (!data.payload) {
                $log.error('invalid message', 'event', event, 'data', JSON.stringify(data));
                return;
            }
            $scope.$apply(function() {
                $scope.messageLog = $scope.messageLog + messageFormatter(new Date(), data.source, data.payload);
            });
        });

        $scope.animationsEnabled = true;
        $scope.openInvitation = function () {

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: '../views/invitation/invitation.html',
                controller: 'InvitationController',
                resolve: {
                    getRoomNumber:function(){
                        return $scope.rnb;
                    }
                }
            });
        };

        $scope.openSaveMeeting = function () {
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: '../views/saveMeeting/saveMeeting.html',
                controller: 'SaveMeetingController',
                resolve: {
                    getRoomNumber:function(){
                        return $scope.rnb;
                    }
                }
            });
        };

        // Take a tour
        $scope.IntroOptions = {
            steps:[
                {
                    element: '#meetingRoomNumber',
                    intro: "Now you have created a meeting room successfully. Here is the <strong>Meeting Room Number</strong> displayed.",
                    position: "down"
                },
                {
                    element: '#invitationBtn',
                    intro: "By clicking the button a pop up page will be displayed where you can copy the <strong>MRN</strong> or invite participants via email",
                    position: "down"
                },
                {
                    element: '#saveMeetingBtn',
                    intro: "If you would like to <strong>Bookmark</strong> the meeting press the button. You have to provide the following data to save" +
                    "<ol>" +
                    "<li>Meeting Name (a name for the meeting)</li>" +
                        "<li>Meeting Description (short description of the meeting)</li> " +
                    "</ol>"
                },
                {
                    element: '#video',
                    intro: "This is your local media! Other participants will be on the right side from you."
                },
                {
                    element: '#loadPreviousMsg',
                    intro: "By clicking the button you will get all the previous messages of the meeting room",
                    position: "left"
                },
                {
                    element: '#chatBox',
                    intro: "In this box all the chat messages will be shown!",
                    position: "left"
                },
                {
                    element: '#chatMsgA',
                    intro: "To chat with other participants you have to provide the message in this input box and press the <strong>Enter</strong> button or click the button <strong>Send</strong>",
                    position: "left"
                },
                {
                    element: '#faqs',
                    intro: "Any questions? Please have a look to our FAQ page.",
                    position: "left"
                }
            ],
            showStepNumbers: false,
            exitOnOverlayClick: true,
            exitOnEsc:true,
            nextLabel: '<strong>NEXT!</strong>',
            prevLabel: '<span style="color:green">Previous</span>',
            skipLabel: 'Exit',
            doneLabel: 'Thanks',
            keyboardNavigation: true,
            showProgress: true
        };
        $scope.ShouldAutoStart = false;
    }
});
