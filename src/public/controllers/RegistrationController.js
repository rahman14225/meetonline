angular.module("meetonline").controller("RegistrationController", function ($scope, $http, $location) {
    if ($scope.isLoggedIn()) {
        $location.path("/dashboard");
    }

    $scope.master = {};

    //hide feedback section
    $scope.isFeedback = "none";

    //After clicking Register button
    $scope.submit = function () {
        if ($scope.regForm.$valid) {
            var uemail = $scope.reg.email;
            var upassword = $scope.reg.password;
            var passwordHashValue = CryptoJS.SHA512(upassword);
            var hashedPassword = passwordHashValue.toString(CryptoJS.enc.Hex);

            var data = {email: uemail, password: hashedPassword};
            var dataEmail = {emailTo: uemail};
            $http.post("/api/auth/registration", data)
                .success(function () {
                    $scope.feedbackState = "success";
                    $scope.isFeedback = "block";
                    $scope.feedbackMsg = "Registration successful";
                    $scope.reg = angular.copy($scope.master);
                    $scope.regForm.$setPristine();
                    $http.post("/api/sendEmailReg", dataEmail)
                })
                .error(function () {
                    $scope.feedbackState = "danger";
                    $scope.isFeedback = "block";
                    $scope.feedbackMsg = "Email is already in use!";
                    $scope.reg = angular.copy($scope.master);
                    $scope.regForm.$setPristine();
                });
        }
    };
});