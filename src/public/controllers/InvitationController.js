angular.module("meetonline").controller("InvitationController", function ($scope, $http, $uibModalInstance, getRoomNumber) {

    $scope.rnb = getRoomNumber;
    $scope.isFeedback = "none";

    $scope.closeModal = function(){
        $uibModalInstance.close();
    };

    // Email invitation
    $scope.sendEmail = function () {
        $scope.masterI = {};
        var data = {emailTo: $scope.invitation.invitationEmail, meetingRoomNumber: getRoomNumber};
        $http.post("/api/sendEmail", data)
            .success(function (data) {
                $scope.invitation = angular.copy($scope.masterI);
                $scope.invitationForm.$setPristine();
                console.log(data);
                $scope.feedbackState = "success";
                $scope.isFeedback = "block";
                $scope.feedbackMsg = "Email sent";
            })
            .error(function () {
                $scope.feedbackState = "danger";
                $scope.isFeedback = "block";
                $scope.feedbackMsg = "It seems to be a problem on Email-Service";
                $scope.invitation = angular.copy($scope.masterI);
                $scope.invitationForm.$setPristine();
            });
    }
});