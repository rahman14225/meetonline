angular.module("meetonline").controller("LoginController", function ($scope, $http, $location, localStorageService) {
    if ($scope.isLoggedIn()) {
        $location.path("/dashboard");
    }

    $scope.master = {};


    //After clicking Login button
    $scope.submit = function () {
        if ($scope.loginForm.$valid) {
            var uemail = $scope.login.email;
            var upassword = $scope.login.password;
            var passwordHashValue = CryptoJS.SHA512(upassword);
            var hashedPassword = passwordHashValue.toString(CryptoJS.enc.Hex);

            var data = {email: uemail, password: hashedPassword};
            $http.post("/api/auth/login", data)
                .success(function (data) {
                    localStorageService.set("user", data);
                    $location.path('/dashboard');
                })
                .error(function () {
                    $scope.loginError = true;
                    $scope.login = angular.copy($scope.master);
                    $scope.loginForm.$setPristine();
                });
        }
    }
});