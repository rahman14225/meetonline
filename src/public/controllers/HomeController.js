angular.module("meetonline").controller("HomeController", function ($scope, $http, $location) {
    if ($scope.isLoggedIn()) {
        $location.path("/dashboard");
    }
    // Take a tour
    $scope.IntroOptions = {
        steps:[
            {
                element: '#mo-logo',
                intro: "MeetOnline is a web based conference system to provide companies as well as private persons to organise their meetings online."
            },
            {
                element: '#registration',
                intro: "If you don't have an account yet, by clicking this button you will be forwarded to the <strong>Registration</strong> site. <strong>or</strong>",
                position: "top"
            },
            {
                element: '#login',
                intro: "If you already have an account, by clicking this button you will be forwarded to the <strong>Login</strong> page.",
                position: "top"
            },
            {
                element: '#faqsH',
                intro: "Any questions? Please have a look to our FAQ page",
                position: "left"
            }
        ],
        showStepNumbers: false,
        exitOnOverlayClick: true,
        exitOnEsc:true,
        nextLabel: '<strong>NEXT!</strong>',
        prevLabel: '<span style="color:green">Previous</span>',
        skipLabel: 'Exit',
        doneLabel: 'Thanks',
        keyboardNavigation: true,
        showProgress: true
    };
    $scope.ShouldAutoStart = false;
});