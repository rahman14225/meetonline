angular.module("meetonline").controller("DashboardController", function ($scope, $http, $location, $route) {
    if (!$scope.isLoggedIn()) {
        $location.path("/");
    }
    $scope.master = {};

    $http.get("/api/meetings")
        .success(function (meetings) {
            $scope.meetings = meetings;
        });

    $scope.submit = function (data) {
        $location.path("/room/"+ $scope.number);
    };

    // Take a tour
    $scope.IntroOptions = {
        steps:[
            {
                element: '#bookmarkedMeetings',
                intro: "In this section you will see all the <strong>Bookmarked Meetings</strong> that you have saved from a meeting session",
                position: "down"
            },
            {
                element: '#joinMeeting',
                intro: "To join a meeting please enter the <strong>Meeting Room Number</strong> here and click the button. (You will get the <strong>Meeting Room Number (MRN)</strong> privately or via email!)",
                position: "down"
            },
            {
                element: '#createRoom',
                intro: "To create a new meeting room please click the following button."
            },
            {
                element: '#myAccountBtn',
                intro: "To change your password please visit <strong>MyAccount</strong> page."
            },
            {
                element: '#logout',
                intro: "To logout from the application just click the <strong>Logout</strong> button.",
                position: "left"
            },
            {
                element: '#faqs',
                intro: "Any questions? Please have a look to our FAQ page.",
                position: "left"
            }
        ],
        showStepNumbers: false,
        exitOnOverlayClick: true,
        exitOnEsc:true,
        nextLabel: '<strong>NEXT!</strong>',
        prevLabel: '<span style="color:green">Previous</span>',
        skipLabel: 'Exit',
        doneLabel: 'Thanks',
        keyboardNavigation: true,
        showProgress: true
    };
    $scope.ShouldAutoStart = false;
});