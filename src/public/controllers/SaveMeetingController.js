angular.module("meetonline").controller("SaveMeetingController", function ($scope, $http, $uibModalInstance, getRoomNumber) {

    $scope.rnb = getRoomNumber;

    $scope.closeModal = function(){
        $uibModalInstance.close();
    };
    $scope.isFeedback = "none";

    // Email invitation
    $scope.saveMeeting = function () {
        $scope.masterI = {};
        var data = {name: $scope.sm.mName, room_nr: getRoomNumber, desc: $scope.sm.mDesc};
        $http.post("/api/meetings", data)
            .success(function (data) {
                $scope.sm = angular.copy($scope.masterI);
                $scope.bookmarkForm.$setPristine();
                $scope.feedbackState = "success";
                $scope.isFeedback = "block";
                $scope.feedbackMsg = "Meeting bookmarked";
            })
            .error(function () {
                $scope.feedbackState = "danger";
                $scope.isFeedback = "block";
                $scope.feedbackMsg = "You already have bookmarked this meeting";
                $scope.sm = angular.copy($scope.masterI);
                $scope.bookmarkForm.$setPristine();
            });
    }
});