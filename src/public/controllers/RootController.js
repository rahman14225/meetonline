angular.module("meetonline").controller("RootController", function($scope, $location, $http, localStorageService, $routeParams, $route, $templateCache) {
    $scope.isLoggedIn = function() {
        return localStorageService.get("user") ? true : false;
    };

    $scope.Logout = function(){
        $http.get("/api/auth/logout")
            .success(function () {
                localStorageService.remove("user");
                $location.path('/login');
            })
            .error(function () {
                $location.path('/dashboard');
            });
    };

    $scope.isRoomNrActive = function(roomId){
        var data = {room_nr: roomId};
        if($http.post("/api/meetingsActive", data).success()){
            return true;
        }else
        {
            return false;
        }
    };
});