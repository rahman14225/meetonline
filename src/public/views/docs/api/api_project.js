define({
  "name": "MeetOnline API",
  "version": "0.1.0",
  "description": "API Documentation of MeetOnline",
  "title": "MeetOnline API Doc",
  "header": {
    "title": "MeetOnline API Documentation"
  },
  "order": [
    "registration",
    "login"
  ],
  "sampleUrl": false,
  "apidoc": "0.2.0",
  "generator": {
    "name": "apidoc",
    "time": "2016-02-15T19:40:06.473Z",
    "url": "http://apidocjs.com",
    "version": "0.15.0"
  }
});
