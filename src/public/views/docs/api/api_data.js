define({ "api": [
  {
    "type": "post",
    "url": "/api/changePassword",
    "title": "Change Password",
    "name": "changePassword",
    "group": "Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email address of the new user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "currentPassword",
            "description": "<p>SHA512 hashed current password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "newPassword",
            "description": "<p>SHA512 hashed new password</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>The user ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>The email address of the user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\nsuccess: 'Password updated'\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>The <code>email</code> and/or <code>password</code> of the user was wrong.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidData",
            "description": "<p>The <code>email</code> and/or <code>password</code> of the new user was invalid.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response (UserNotFound):",
          "content": "HTTP/1.1 400 Bad Request\n{\nerror: 'No user found'\n}",
          "type": "json"
        },
        {
          "title": "Error-Response (InvalidData):",
          "content": "HTTP/1.1 400 Bad Request\n{\nerror: \"Invalid email address or password\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./auth.js",
    "groupTitle": "Authentication"
  },
  {
    "type": "get",
    "url": "/api/auth/loggedIn",
    "title": "LoggedIn",
    "name": "loggedIn",
    "group": "Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Session",
            "optional": false,
            "field": "user",
            "description": "<p>User session on the server-side</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Session",
            "optional": false,
            "field": "user",
            "description": "<p>The user session is active</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n'Already logged in'\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./auth.js",
    "groupTitle": "Authentication"
  },
  {
    "type": "post",
    "url": "/api/auth/login",
    "title": "Login",
    "name": "login",
    "group": "Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email address of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>SHA512 hashed password</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>The user ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>The email address of the user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\nid: 14,\nemail: \"mrahman3@uclan.ac.uk\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "WrongData",
            "description": "<p>The <code>email</code> and/or <code>password</code> of the user was wrong.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidData",
            "description": "<p>The <code>email</code> and/or <code>password</code> of the new user was invalid.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response (WrongData):",
          "content": "HTTP/1.1 400 Bad Request\n{\nerror: \"Wrong email/password\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response (InvalidData):",
          "content": "HTTP/1.1 400 Bad Request\n{\nerror: \"Invalid email address or password\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./auth.js",
    "groupTitle": "Authentication"
  },
  {
    "type": "get",
    "url": "/api/auth/logout",
    "title": "Logout",
    "name": "logout",
    "group": "Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Session",
            "optional": false,
            "field": "user",
            "description": "<p>User session on the server-side</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Session",
            "optional": false,
            "field": "user",
            "description": "<p>The user session was deleted</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n'logged out'\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./auth.js",
    "groupTitle": "Authentication"
  },
  {
    "type": "post",
    "url": "/api/auth/registration",
    "title": "Registration",
    "name": "registration",
    "group": "Authentication",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email address of the new user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>SHA512 hashed password</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>The created user ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>The email address of the new user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\nid: 14,\nemail: \"mrahman3@uclan.ac.uk\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserAlreadyExists",
            "description": "<p>The <code>email</code> of the new user was found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidData",
            "description": "<p>The <code>email</code> and/or <code>password</code> of the new user was invalid.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response (UserAlreadyExists):",
          "content": "HTTP/1.1 400 Bad Request\n{\nid: 14,\nemail: \"mrahman3@uclan.ac.uk\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response (InvalidData):",
          "content": "HTTP/1.1 400 Bad Request\n{\nerror: \"Invalid email address or password\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./auth.js",
    "groupTitle": "Authentication"
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./doc/main.js",
    "group": "C__Users_Mahmud_Documents_Privat_UDA_UCLAN_MeetOnline_Dev_src_api_doc_main_js",
    "groupTitle": "C__Users_Mahmud_Documents_Privat_UDA_UCLAN_MeetOnline_Dev_src_api_doc_main_js",
    "name": ""
  },
  {
    "type": "post",
    "url": "/api/messages",
    "title": "GetMessages",
    "name": "getMessages",
    "group": "Chat",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "room_nr",
            "description": "<p>Meeting Room Number</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>User ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email address of the user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>The chat message</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "room_nr",
            "description": "<p>Meeting Room Number</p>"
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "createdAt",
            "description": "<p>DB insert date</p>"
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "updatedAt",
            "description": "<p>DB update date</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n\"messages\": [\n{\n\"id\": 1,\n\"email\": \"m@m.com\",\n\"message\": \"Hi\",\n\"room_nr\": \"ac6ca38b-f37f-4e7f-9da9-08920811eac7\",\n\"createdAt\": \"2016-01-25T18:28:32.000Z\",\n\"updatedAt\": \"2016-01-25T18:28:32.000Z\"\n}\n]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoPreviousMessages",
            "description": "<p>The <code>room_nr</code> does not have any chat messages.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\nerror: \"No data found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./chat.js",
    "groupTitle": "Chat"
  },
  {
    "type": "post",
    "url": "/api/messageInsert",
    "title": "Insert Message",
    "name": "insertMessage",
    "group": "Chat",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email address of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Chat message</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "room_nr",
            "description": "<p>Meeting Room Number</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>User ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email address of the user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>The chat message</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "room_nr",
            "description": "<p>Meeting Room Number</p>"
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "createdAt",
            "description": "<p>DB insert date</p>"
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "updatedAt",
            "description": "<p>DB update date</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n\"messages\": [\n{\n\"id\": 1,\n\"email\": \"m@m.com\",\n\"message\": \"Hi\",\n\"room_nr\": \"ac6ca38b-f37f-4e7f-9da9-08920811eac7\",\n\"createdAt\": \"2016-01-25T18:28:32.000Z\",\n\"updatedAt\": \"2016-01-25T18:28:32.000Z\"\n}\n]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "DoubleMessage",
            "description": "<p>The requested data is not unique.</p>"
          }
        ]
      }
    },
    "filename": "./chat.js",
    "groupTitle": "Chat"
  },
  {
    "type": "post",
    "url": "/api/sendEmail",
    "title": "Invitation Email",
    "name": "invitation",
    "group": "Mailer",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "emailTo",
            "description": "<p>Recipient email address</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "meetingRoomNumber",
            "description": "<p>Meeting Room Number for the invitation</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n'Email sent'\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ServerError",
            "description": "<p>Internal server error</p>"
          }
        ]
      }
    },
    "filename": "./mailer.js",
    "groupTitle": "Mailer"
  },
  {
    "type": "post",
    "url": "/api/sendEmailReg",
    "title": "Registration Email",
    "name": "registrationEmail",
    "group": "Mailer",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "emailTo",
            "description": "<p>Recipient email address</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n'Email sent'\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ServerError",
            "description": "<p>Internal server error</p>"
          }
        ]
      }
    },
    "filename": "./mailer.js",
    "groupTitle": "Mailer"
  },
  {
    "type": "post",
    "url": "/api/meetingsActive",
    "title": "Active Meetings",
    "name": "activeMeetings",
    "group": "Meeting",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "room_nr",
            "description": "<p>Meeting Room Number</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Meeting Room Name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "room_nr",
            "description": "<p>Meeting Room Number</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "desc",
            "description": "<p>Meeting Room Description</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "user",
            "description": "<p>Bookmarked by user</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "active",
            "description": "<p>Is the meeting room active?</p>"
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "createdAt",
            "description": "<p>DB insert date</p>"
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "updatedAt",
            "description": "<p>DB update date</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n\"name\": \"Test Meeting\",\n\"room_nr\": \"ac6ca38b-f37f-4e7f-9da9-08920811eac7\",\n\"desc\": \"Test Test Test Test \",\n\"user\": 1,\n\"active\": 1,\n\"createdAt\": \"2016-01-25T18:28:32.000Z\",\n\"updatedAt\": \"2016-01-25T18:28:32.000Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "DataNotFound",
            "description": "<p>The given <code>room_nr</code> not found in the DB</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\nerror: \"No data found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./meeting.js",
    "groupTitle": "Meeting"
  },
  {
    "type": "post",
    "url": "/api/meetings",
    "title": "Bookmark meeting",
    "name": "bookmarkMeeting",
    "group": "Meeting",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Meeting Room Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "room_nr",
            "description": "<p>Meeting Room Number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "desc",
            "description": "<p>Meeting Room Description</p>"
          },
          {
            "group": "Parameter",
            "type": "Session",
            "optional": false,
            "field": "user",
            "description": "<p>Server-side session user id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Meeting Room Name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "room_nr",
            "description": "<p>Meeting Room Number</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "desc",
            "description": "<p>Meeting Room Description</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "user",
            "description": "<p>Bookmarked by user</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "active",
            "description": "<p>Is the meeting room active?</p>"
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "createdAt",
            "description": "<p>DB insert date</p>"
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "updatedAt",
            "description": "<p>DB update date</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n\"name\": \"Test Meeting\",\n\"room_nr\": \"ac6ca38b-f37f-4e7f-9da9-08920811eac7\",\n\"desc\": \"Test Test Test Test \",\n\"user\": 1,\n\"active\": 1,\n\"createdAt\": \"2016-01-25T18:28:32.000Z\",\n\"updatedAt\": \"2016-01-25T18:28:32.000Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Uniqueness",
            "description": "<p>The requested data is not unique</p>"
          }
        ]
      }
    },
    "filename": "./meeting.js",
    "groupTitle": "Meeting"
  },
  {
    "type": "get",
    "url": "/api/meetings",
    "title": "Get Meetings",
    "name": "getMeetings",
    "group": "Meeting",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Session",
            "optional": false,
            "field": "userID",
            "description": "<p>Server-side session (user ID)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Meeting Room Name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "room_nr",
            "description": "<p>Meeting Room Number</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "desc",
            "description": "<p>Meeting Room Description</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "user",
            "description": "<p>Bookmarked by user</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "active",
            "description": "<p>Is the meeting room active?</p>"
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "createdAt",
            "description": "<p>DB insert date</p>"
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "updatedAt",
            "description": "<p>DB update date</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n\"meetings\": [\n{\n\"name\": \"Test Meeting\",\n\"room_nr\": \"ac6ca38b-f37f-4e7f-9da9-08920811eac7\",\n\"desc\": \"Test Test Test Test \",\n\"user\": 1,\n\"active\": 1,\n\"createdAt\": \"2016-01-25T18:28:32.000Z\",\n\"updatedAt\": \"2016-01-25T18:28:32.000Z\"\n}\n]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "DataNotFound",
            "description": "<p>There are not any bookmarked meetings for the user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\nerror: \"No data found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./meeting.js",
    "groupTitle": "Meeting"
  }
] });
