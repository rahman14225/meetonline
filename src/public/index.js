var app = angular.module("meetonline", ["ngRoute", "LocalStorageModule", "angular-intro", "validation.match", "ngCookies", "ngResource", "ngSanitize", "btford.socket-io", "ngclipboard", "ui.bootstrap"])
    .value('nickName', 'anonymous');

app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: "views/home/home.html"
        })
        .when('/login', {
            templateUrl: "views/login/login.html",
            controller: "LoginController"
        })
        .when('/registration', {
            templateUrl: "views/registration/registration.html",
            controller: "RegistrationController"
        })
        .when('/dashboard', {
            templateUrl: "views/dashboard/dashboard.html",
            controller: "DashboardController"
        })
        .when('/room', {
            templateUrl: "views/meeting/meeting.html",
                controller: "RoomCtrl"
        })
        .when('/room/:roomId', {
            templateUrl: "views/meeting/meeting.html",
            controller: "RoomCtrl"
        })
        .when('/myAccount', {
            templateUrl: "views/myAccount/myAccount.html",
            controller: "MyAccountController"
        })
        .when('/faq', {
            templateUrl: "views/faq/faq.html"
        })
        .when('/test', {
            templateUrl: "views/test/test.html",
            controller: "PhoneListCtrl"
        })
        .otherwise({
            redirectTo: '/'
        });
}]);
angular.module('meetonline').constant('config', {
        SIGNALIG_SERVER_URL: undefined
    });

Object.setPrototypeOf = Object.setPrototypeOf || function(obj, proto) {
    obj.__proto__ = proto;
    return obj;
};