'use strict';

angular.module('meetonline').value('messageFormatter', function (date, nick, message) {
    return date.toLocaleTimeString() + ' - ' +
        nick + ' - ' +
        message + '\n';
});
