//Sources
var Https = require('https');
var Http = require('http');
var Fs = require('fs');
var IP = require('ip');
var Config = require('./../config/config.json');

var Express = require("express");
var app = Express();
// create server
var Server;
if(Config.debug){
    Server = Http.createServer(app);
} else {
    Server = Https.createServer({
        key: Fs.readFileSync('./server/key.pem'),
        cert: Fs.readFileSync('./server/cert.pem')
    },app);
}

var Session = require('express-session');
var BodyParser = require('body-parser');
var Socketio = require('socket.io');
Socketio = Socketio.listen(Server, {log: false});

//special routes
app.use(Express.static(__dirname + '/../src/public/'));

app.use(BodyParser.json());
app.use(Session({
    secret: 'meetonline-123',
    resave: false,
    saveUninitialized: true
}));

// Mock
app.use(function (req, res, next) {
    req.session.user = {
        email: 'm@m.com',
        password: 'rahman',
        id: 1
    };
    return next();
});

// run the server
exports.start = function (config) {

    // Routes
    require("../src/api/auth")(app);
    require("../src/api/meeting")(app);
    require("../src/api/chat")(app);
    require("../src/api/mailer") (app);

    Server.listen(config.PORT);
    console.log(config.IP +' listening on: '+ config.PORT);
    console.log(IP.address());

    //socket routes
    require("../sockets/StreamSocket")(Socketio);
    require("../sockets/chatSocket")(Socketio);

};