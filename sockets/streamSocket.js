var Uuid = require('node-uuid');
var Meetings = {};
var Participants = {};

var LogConf = require('../config/log4js-conf.json');
var log4js = require('log4js');

log4js.configure(LogConf, {});

var streamSocketLogger = log4js.getLogger('streamSocket');

module.exports = function (Socketio) {
    Socketio.on('connection', function (socket) {

        var currentMeeting;
        var participant;

        socket.on('init', function (data, fn) {
            currentMeeting = (data || {}).room || Uuid.v4();
            var room = Meetings[currentMeeting];
            if (!data) {
                Meetings[currentMeeting] = [socket];
                participant = Participants[currentMeeting] = 1;
                fn(currentMeeting, participant);
                streamSocketLogger.info('Meeting created:', currentMeeting);
            } else {
                if (!room) {
                    return;
                }
                Participants[currentMeeting] += 1;
                participant = Participants[currentMeeting];
                fn(currentMeeting, participant);
                room.forEach(function (joinCon) {
                    joinCon.emit('peer.connected', {id: participant});
                });
                room[participant] = socket;
                streamSocketLogger.info('Peer connected to meeting', currentMeeting);
                console.log('Peer connected to meeting', currentMeeting, 'with #', participant);
            }
        });

        socket.on('disconnect', function () {
            if (!currentMeeting || !Meetings[currentMeeting]) {
                return;
            }
            Meetings[currentMeeting].forEach(function (socket) {
                if (socket) {
                    socket.emit('peer.disconnected', {id: participant});
                }
            });
        });

        socket.on('msg', function (data) {
            var towards = parseInt(data.to, 10);
            if (Meetings[currentMeeting] && Meetings[currentMeeting][towards]) {
                Meetings[currentMeeting][towards].emit('msg', data);
            } else {
                streamSocketLogger.warn('Invalid user');
            }
        });
    });
};