var LogConf = require('../config/log4js-conf.json');
var log4js = require('log4js');

log4js.configure(LogConf, {});

var chatSocketLogger = log4js.getLogger('chatSocket');

module.exports = function (Socketio) {
    'use strict';
    Socketio.on('connection', function (socket) {
        socket.broadcast.emit('user connected');

        socket.on('message', function (from, msg) {
            chatSocketLogger.info('Recieved message from', from);
            Socketio.sockets.emit('broadcast', {
                payload: msg,
                source: from
            });
        });
    });
};