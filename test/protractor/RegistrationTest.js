describe('MeetOnline Registration Process', function () {
    describe('Successful registration', function () {
        describe('MeetOnline Homepage', function () {
            it('Click on Registration button', function () {
                browser.get('/');
                expect(browser.getTitle()).toEqual('MeetOnline');
                browser.sleep(1500);
                element(by.id('registration')).click();
                browser.sleep(1500);
            });
        });

        describe('MeetOnline Registration', function () {
            var email = element(by.model('reg.email'));
            var password = element(by.model('reg.password'));
            var coPassword = element(by.model('reg.coPassword'));
            var registerBtn = element(by.id('registrationBtn'));

            it('Expected title and URL', function () {
                expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/registration');
                expect(browser.getTitle()).toEqual('MeetOnline');
            });
            it('Fill up the form with validated data', function () {
                email.sendKeys("test@protractor.com");
                password.sendKeys("rahman");
                coPassword.sendKeys("rahman");
                browser.sleep(1500);
            });
            it('Click the Register button', function () {
                registerBtn.click();
                browser.sleep(1500);
            });
            it('Get feedback success', function () {
                expect(element(by.binding('feedbackMsg')).getText()).
                toEqual('Registration successful');
                browser.sleep(1500);
            });
        });
    });

    describe('Unsuccessful registration', function () {
        describe('MeetOnline Homepage', function () {
            it('Click on Registration button', function () {
                browser.get('/');
                expect(browser.getTitle()).toEqual('MeetOnline');
                browser.sleep(1500);
                element(by.id('registration')).click();
                browser.sleep(1500);
            });
        });

        describe('MeetOnline Registration', function () {
            var email = element(by.model('reg.email'));
            var password = element(by.model('reg.password'));
            var coPassword = element(by.model('reg.coPassword'));
            var registerBtn = element(by.id('registrationBtn'));

            it('Expected title and URL', function () {
                expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/registration');
                expect(browser.getTitle()).toEqual('MeetOnline');
            });
            it('Fill up the form with validated data', function () {
                email.sendKeys("test@protractor.com");
                password.sendKeys("rahman");
                coPassword.sendKeys("rahman");
                browser.sleep(1500);
            });
            it('Click the Register button', function () {
                registerBtn.click();
                browser.sleep(1500);
            });
            it('Get feedback success', function () {
                expect(element(by.binding('feedbackMsg')).getText()).
                toEqual('Email is already in use!');
                browser.sleep(1500);
            });
        });
    });
});

