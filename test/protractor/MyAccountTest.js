describe('MeetOnline MyAccount Process', function () {
    describe('Successful password change', function () {
        describe('MeetOnline Homepage', function () {
            it('Click on Login button', function () {
                browser.get('/');
                expect(browser.getTitle()).toEqual('MeetOnline');
                browser.sleep(1500);
                element(by.id('login')).click();
                browser.sleep(1500);
            });
        });

        describe('MeetOnline Login', function () {
            var email = element(by.model('login.email'));
            var password = element(by.model('login.password'));
            var loginBtn = element(by.id('loginBtn'));

            it('Expected title and URL', function () {
                expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/login');
                expect(browser.getTitle()).toEqual('MeetOnline');
            });
            it('Fill up the form with validated data', function () {
                email.sendKeys("test@protractor.com");
                password.sendKeys("rahman");
                browser.sleep(1500);
            });
            it('Click the Register button', function () {
                loginBtn.click();
                browser.sleep(1500);
            });
            it('Get feedback', function () {
                expect(element(by.className('alert-danger')).isDisplayed()).toBe(false);
                browser.sleep(1500);
            });
            it('Go to the MyAccount Page', function () {
                var myAccountBtn = element(by.id('myAccountBtn'));
                myAccountBtn.click();
            });

            describe('MeetOnline MyAccount', function () {
                var currentPassword = element(by.model('myAcc.password'));
                var newPassword = element(by.model('myAcc.newPassword'));
                var coNewPassword = element(by.model('myAcc.coPassword'));
                var changePasswordBtn = element(by.id('changePasswordBtn'));

                it('Expected title and URL', function () {
                    expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/myAccount');
                    expect(browser.getTitle()).toEqual('MeetOnline');
                });
                it('Fill up the form to change the password', function () {
                    currentPassword.sendKeys("rahman");
                    newPassword.sendKeys("rahman14");
                    coNewPassword.sendKeys("rahman14");
                });
                it('Click the Change Password button', function () {
                    changePasswordBtn.click();
                    browser.sleep(1500);
                });
                it('Get feedback success', function () {
                    expect(element(by.binding('feedbackMsg')).getText()).toEqual('Password changed');
                    browser.sleep(1500);
                });
            });
        });
    });

    describe('Unsuccessful password change', function () {
        it('Go to MyAccount', function () {
            browser.get('/#/myAccount');
            expect(browser.getTitle()).toEqual('MeetOnline');
            browser.sleep(1500);
        });

        describe('MeetOnline MyAccount', function () {
            var currentPassword = element(by.model('myAcc.password'));
            var newPassword = element(by.model('myAcc.newPassword'));
            var coNewPassword = element(by.model('myAcc.coPassword'));
            var changePasswordBtn = element(by.id('changePasswordBtn'));

            it('Expected title and URL', function () {
                expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/myAccount');
                expect(browser.getTitle()).toEqual('MeetOnline');
            });
            it('Fill up the form to change the password', function () {
                currentPassword.sendKeys("rahman");
                newPassword.sendKeys("rahman14");
                coNewPassword.sendKeys("rahman14");
            });
            it('Click the Change Password button', function () {
                changePasswordBtn.click();
                browser.sleep(1500);
            });
            it('Get feedback failure', function () {
                expect(element(by.binding('feedbackMsg')).getText()).toEqual('Current password is incorrect');
                browser.sleep(1500);
            });
        });
    });
});

