describe('MeetOnline Login Process', function () {
    describe('Successful login', function () {
        describe('MeetOnline Homepage', function () {
            it('Click on Registration button', function () {
                browser.get('/');
                expect(browser.getTitle()).toEqual('MeetOnline');
                browser.sleep(1500);
                element(by.id('login')).click();
                browser.sleep(1500);
            });
        });

        describe('MeetOnline Login', function () {
            var email = element(by.model('login.email'));
            var password = element(by.model('login.password'));
            var loginBtn = element(by.id('loginBtn'));

            it('Expected title and URL', function () {
                expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/login');
                expect(browser.getTitle()).toEqual('MeetOnline');
            });
            it('Fill up the form with validated data', function () {
                email.sendKeys("test@protractor.com");
                password.sendKeys("rahman");
                browser.sleep(1500);
            });
            it('Click the Register button', function () {
                loginBtn.click();
                browser.sleep(1500);
            });
            it('Get feedback', function () {
                expect(element(by.className('alert-danger')).isDisplayed()).toBe(false);
                browser.sleep(1500);
            });
            it('Logout', function () {
                var logout = element(by.id('logout'));
                logout.click();
                browser.sleep(1500);
            });
        });
    });

    describe('Unsuccessful login', function () {
        describe('MeetOnline Homepage', function () {
            it('Click on Registration button', function () {
                browser.get('/');
                expect(browser.getTitle()).toEqual('MeetOnline');
                browser.sleep(1500);
                element(by.id('login')).click();
                browser.sleep(1500);
            });
        });

        describe('MeetOnline Login', function () {
            var email = element(by.model('login.email'));
            var password = element(by.model('login.password'));
            var loginBtn = element(by.id('loginBtn'));

            it('Expected title and URL', function () {
                expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/login');
                expect(browser.getTitle()).toEqual('MeetOnline');
            });
            it('Fill up the form with validated data', function () {
                email.sendKeys("test@protractor.com");
                password.sendKeys("rahman1");
                browser.sleep(1500);
            });
            it('Click the Register button', function () {
                loginBtn.click();
                browser.sleep(1500);
            });
            it('Get feedback', function () {
                expect(element(by.className('alert-danger')).isDisplayed()).toBe(true);
                browser.sleep(1500);
            });
        });
    });
});

