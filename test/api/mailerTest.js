var supertest = require("supertest");
var chai = require('chai');
var should = chai.should();

var server = supertest.agent("http://localhost:8080");

describe("Mailer Tests", function () {
    this.timeout(9000);

    it("Send an email with all params", function (done) {
        server
            .post('/api/sendEmail')
            .send({
                emailTo: 'rahman_mahmudur4@hotmail.com',
                meetingRoomNumber: '79551e2c-5d6a-4537-9c20-29acbc41e83m'
            })
            .expect("Content-type", /json/)
            .expect(200)
            .end(function (err, res) {
                res.status.should.equal(200);
                res.body.should.be.a('object');
                done();
            });
    });

    it("Send an email without email address", function (done) {
        server
            .post('/api/sendEmail')
            .send({
                emailTo: null,
                meetingRoomNumber: '79551e2c-5d6a-4537-9c20-29acbc41e83m'
            })
            .expect("Content-type", /json/)
            .expect(400)
            .end(function (err, res) {
                res.status.should.equal(400);
                res.body.should.be.a('object');
                done();
            });
    });

    it("Send an email without meeting room number", function (done) {
        server
            .post('/api/sendEmail')
            .send({
                emailTo: 'rahman_mahmudur4@hotmail.com',
                meetingRoomNumber: null
            })
            .expect("Content-type", /json/)
            .expect(200)
            .end(function (err, res) {
                res.status.should.equal(200);
                res.body.should.be.a('object');
                done();
            });
    });

    it("Send an email without params", function (done) {
        server
            .post('/api/sendEmail')
            .send({
                emailTo: null,
                meetingRoomNumber: null
            })
            .expect("Content-type", /json/)
            .expect(400)
            .end(function (err, res) {
                res.status.should.equal(400);
                res.body.should.be.a('object');
                done();
            });
    });
});