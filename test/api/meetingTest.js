var supertest = require("supertest");
//var should = require("should");
var chai = require('chai');
var should = chai.should();

var server = supertest.agent("http://localhost:8080");

describe("Meeting Tests", function () {

    it("Create meeting", function (done) {
        server
            .post('/api/meetings')
            .send({
                name: 'Test Meeting',
                room_nr: 'ac6ca38b-f37f-4e7f-9da9-08920811eac7',
                desc: 'Test Meeting created by Testing Team'
            })
            .expect("Content-type", /json/)
            .expect(200)
            .end(function (err, res) {
                res.status.should.equal(200);
                res.body.should.be.a('object');
                done();
            });
    });

    it("Create meeting with existing room number", function (done) {
        server
            .post('/api/meetings')
            .send({
                name: 'Test Meeting',
                room_nr: '79551e2c-5d6a-4537-9c20-29acbc41e83m',
                desc: 'Test Meeting created by Testing Team'
            })
            .expect("Content-type", /json/)
            .expect(400)
            .end(function (err, res) {
                res.status.should.equal(400);
                res.body.should.be.a('object');
                done();
            });
    });

    it("Get all meetings", function (done) {
        server
            .get('/api/meetings')
            .expect("Content-type", /json/)
            .expect(200)
            .end(function (err, res) {
                res.status.should.equal(200);
                res.body.should.be.a('object');
                done();
            });
    });

    it("Get meeting by room number if active", function (done) {
        server
            .post('/api/meetingsActive')
            .send({room_nr: '79551e2c-5d6a-4537-9c20-29acbc41e83m'})
            .expect("Content-type", /json/)
            .expect(200)
            .end(function (err, res) {
                res.status.should.equal(200);
                res.body.should.be.a('object');
                done();
            });
    });

    it("Get meeting by false room number if active", function (done) {
        server
            .post('/api/meetingsActive')
            .send({room_nr: '79551e2c-5d6a-4537-9c20-29acbc41e83f'})
            .expect("Content-type", /json/)
            .expect(400)
            .end(function (err, res) {
                res.status.should.equal(400);
                res.body.should.be.a('object');
                res.body.error.should.equal('No data found');
                done();
            });
    });

    it("Set meeting active", function (done) {
        server
            .post('/api/meetingSet')
            .send({active: 1, room_nr: 'jhjashdkjas-jhsajdhkas-hkjashdka'})
            .expect("Content-type", /json/)
            .expect(200)
            .end(function (err, res) {
                res.status.should.equal(200);
                done();
            });
    });
});