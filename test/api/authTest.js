var supertest = require("supertest");
//var should = require("should");
var chai = require('chai');
var should = chai.should();

var server = supertest.agent("http://localhost:8080");

describe("Authentication Tests", function () {

    it("Login with existing account", function (done) {
        server
            .post('/api/auth/login')
            .send({email: 'mrahman3@uclan.ac.uk', password: 'a31874d0f57a6a20f520a8bb3a31e1ef0665550e39b3408057b3bb3aa4232dcb768cf60bf8a29b811f0d61a7a8d4039453aed91c8dd3694cbead0d738bdb9db2'})
            .expect("Content-type", /json/)
            .expect(200)
            .end(function (err, res) {
                res.status.should.equal(200);
                res.body.should.be.a('object');
                console.log(err);
                done();
            });
    });

    it("Login with right email, false password", function (done) {
        server
            .post('/api/auth/login')
            .send({email: 'mrahman3@uclan.ac.uk', password: 'meetonline'})
            .expect("Content-type", /json/)
            .expect(400)
            .end(function (err, res) {
                res.status.should.equal(400);
                res.body.error.should.equal('Wrong email/password');
                done();
            });
    });

    it("Login with false email, right password", function (done) {
        server
            .post('/api/auth/login')
            .send({email: 'testing@mocha.com', password: 'a31874d0f57a6a20f520a8bb3a31e1ef0665550e39b3408057b3bb3aa4232dcb768cf60bf8a29b811f0d61a7a8d4039453aed91c8dd3694cbead0d738bdb9db2'})
            .expect("Content-type", /json/)
            .expect(400)
            .end(function (err, res) {
                res.status.should.equal(400);
                res.body.error.should.equal('Wrong email/password');
                done();
            });
    });

    it("Registration with new email", function (done) {
        server
            .post('/api/auth/registration')
            .send({email: 'testing@mocha.com', password: 'a31874d0f57a6a20f520a8bb3a31e1ef0665550e39b3408057b3bb3aa4232dcb768cf60bf8a29b811f0d61a7a8d4039453aed91c8dd3694cbead0d738bdb9db2'})
            .expect("Content-type", /json/)
            .expect(200)
            .end(function (err, res) {
                res.status.should.equal(200);
                done();
            });
    });

    it("Registration with existing account", function (done) {
        server
            .post('/api/auth/registration')
            .send({email: 'mrahman3@uclan.ac.uk', password: 'a31874d0f57a6a20f520a8bb3a31e1ef0665550e39b3408057b3bb3aa4232dcb768cf60bf8a29b811f0d61a7a8d4039453aed91c8dd3694cbead0d738bdb9db2'})
            .expect("Content-type", /json/)
            .expect(400)
            .end(function (err, res) {
                res.status.should.equal(400);
                done();
            });
    });

    it("Logout", function (done) {
        server
            .get('/api/auth/logout')
            .expect("Content-type", /json/)
            .end(function (err, res) {
                res.status.should.equal(200);
                res.body.should.be.a('object');
                done();
            });
    });

    it("Change password with correct data", function (done) {
        server
            .post('/api/changePassword')
            .send({userEmail: 'testing@mocha.com', currentPassword: 'a31874d0f57a6a20f520a8bb3a31e1ef0665550e39b3408057b3bb3aa4232dcb768cf60bf8a29b811f0d61a7a8d4039453aed91c8dd3694cbead0d738bdb9db2', newPassword: '8a1d2f4fe667ecabda8ea91db8b6eab32b12d7d54e4fda3bc3039ab77c7985f3373f8b35005aea85dcf90d16eea12bef78a03eb1f7ea2c4cf77206b4b4ee5f65'})
            .expect("Content-type", /json/)
            .expect(200)
            .end(function (err, res) {
                res.status.should.equal(200);
                done();
            });
    });

    it("Change password with incorrect current password", function (done) {
        server
            .post('/api/changePassword')
            .send({userEmail: 'testing@mocha.com', currentPassword: 'a31874d0f57a6a20f520a8bb3a31e1ef0665550e39b3408057b3bb3aa4232dcb768cf60bf8a29b811f0d61a7a8d4039453aed91c8dd3694cbead0d738bdb9db3', newPassword: '8a1d2f4fe667ecabda8ea91db8b6eab32b12d7d54e4fda3bc3039ab77c7985f3373f8b35005aea85dcf90d16eea12bef78a03eb1f7ea2c4cf77206b4b4ee5f65'})
            .expect("Content-type", /json/)
            .expect(400)
            .end(function (err, res) {
                res.status.should.equal(400);
                done();
            });
    });
});