var supertest = require("supertest");
//var should = require("should");
var chai = require('chai');
var should = chai.should();

var server = supertest.agent("http://localhost:8080");

describe("Chat Tests", function () {

    it("Get messages from a meeting", function (done) {
        server
            .post('/api/messages')
            .send({room_nr: 'ac6ca38b-f37f-4e7f-9da9-08920811eac7'})
            .expect("Content-type", /json/)
            .expect(200)
            .end(function (err, res) {
                res.status.should.equal(200);
                res.body.should.be.a('object');
                done();
            });
    });

    it("Get messages from a meeting without chat", function (done) {
        server
            .post('/api/messages')
            .send({room_nr: 'jhjashdkjas-jhsajdhkas-hkjashdka'})
            .expect("Content-type", /json/)
            .expect(400)
            .end(function (err, res) {
                res.statusCode.should.equal(400);
                res.body.should.be.a('object');
                done();
            });
    });

    it("Insert a massage from chat", function (done) {
        server
            .post('/api/messageInsert')
            .send({
                email: 'testing@mocha.com',
                message: 'Test message from Testing Team',
                room_nr: 'ac6ca38b-f37f-4e7f-9da9-08920811eac7'
            })
            .expect("Content-type", /json/)
            .expect(200)
            .end(function (err, res) {
                res.status.should.equal(200);
                res.body.should.be.a('object');
                done();
            });
    });

    it("Insert a massage without room number", function (done) {
        server
            .post('/api/messageInsert')
            .send({
                email: 'testing@mocha.com',
                message: 'Test message from Testing Team',
                room_nr: null
            })
            .expect("Content-type", /json/)
            .expect(400)
            .end(function (err, res) {
                res.status.should.equal(400);
                res.body.should.be.a('object');
                done();
            });
    });
});