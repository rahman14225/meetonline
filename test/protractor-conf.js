var HtmlReporter = require('protractor-html-screenshot-reporter');
// An example configuration file.
exports.config = {
    //directConnect: true,
    seleniumAddress: 'http://localhost:4444/wd/hub',
    baseUrl: "http://localhost:8080",
    // Capabilities to be passed to the webdriver instance.
    capabilities: {
        'browserName': 'chrome'
    },

    // Framework to use. Jasmine is recommended.
    framework: 'jasmine',
    // Spec patterns are relative to the current working directly when
    // protractor is called.
    specs: ['protractor/RegistrationTest.js'/*, 'protractor/LoginTest.js', 'protractor/MyAccountTest.js'*/],

    onPrepare: function() {
        // Add a screenshot reporter:
        jasmine.getEnv().addReporter(new HtmlReporter({
            baseDirectory: 'screenshots',
            takeScreenShotsOnlyForFailedSpecs: true,
            pathBuilder: function pathBuilder(spec, descriptions, results, capabilities) {

                var monthMap = {
                    "1": "Jan",
                    "2": "Feb",
                    "3": "Mar",
                    "4": "Apr",
                    "5": "May",
                    "6": "Jun",
                    "7": "Jul",
                    "8": "Aug",
                    "9": "Sep",
                    "10": "Oct",
                    "11": "Nov",
                    "12": "Dec"
                };

                var currentDate = new Date(),
                    currentHoursIn24Hour = currentDate.getHours(),
                    currentTimeInHours = currentHoursIn24Hour>12? currentHoursIn24Hour-12: currentHoursIn24Hour,
                    totalDateString = currentDate.getDate()+'-'+ monthMap[currentDate.getMonth()+1]+ '-'+(currentDate.getYear()+1900) +
                        '-'+ currentTimeInHours+'h-' + currentDate.getMinutes()+'m';

                return path.join(totalDateString,capabilities.caps_.browserName, descriptions.join('-'));
            }
        }));
    },

    // Options to be passed to Jasmine.
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 30000
    }
};
